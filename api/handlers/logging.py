"""This is module logging handler"""

import time

from rest_framework.decorators import api_view, permission_classes

from api.models.logging import Logging
from api.utils.auth import AuthenticatedOnly
from api.utils.response import response_error, response_success


@api_view(['POST', 'GET'])
@permission_classes([AuthenticatedOnly])
def index(request):
    # List Logging
    if request.method == 'GET':
        loggings = Logging.objects.filter(
            gateway_serial_id=request.user['gateway_serial_id']
        )
        [print(l.issued_at, l.power) for l in loggings]
        return response_success()

    # Create Logging
    elif request.method == 'POST':
        data = request.data
        try:
            for record in data:
                issued_at = time.strftime(
                    '%Y-%m-%d %H:%M:%S',
                    time.gmtime(float(record['issued_at'])/1000)
                )
                logging = Logging(
                    issued_at=issued_at,
                    gateway_serial_id=request.user['gateway_serial_id'], 
                    power=record['power']
                )
                logging.save()
            return response_success(data)
        except Exception as e:
            return response_error(e.__str__())
