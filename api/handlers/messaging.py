"""This is module asynchronous AMQP messaging handler"""

import json

from api.models.rule import Rule
from api.models.schedule import Schedule
from api.serializers.logging import LoggingSerializer
from api.serializers.rule import RuleSerializer
from api.serializers.schedule import ScheduleSerializer


def handle_response(data, correlation_id, routing_key):
    gateway_serial_id = routing_key[0]
    identifier = routing_key[1]

    if identifier == 'schedules' and correlation_id is not None:
        schedule = Schedule.objects.get(id=correlation_id)
        schedule.schedule_id = data[0]['success']['id']
        schedule.schedule_state = 'active'

        serializer = ScheduleSerializer(schedule)

        if serializer.is_valid():
            serializer.save()

    elif identifier == 'rules' and correlation_id is not None:
        rule = Rule.objects.get(correlation_id)
        rule.rule_id = data[0]['success']['id']
        rule.rule_state = 'active'

        serializer = RuleSerializer(rule)

        if serializer.is_valid():
            serializer.save()

    elif identifier == 'logging':
        data['gateway_serial_id'] = gateway_serial_id

        serializer = LoggingSerializer(data=json.dumps(data))

        if serializer.is_valid():
            serializer.save()
