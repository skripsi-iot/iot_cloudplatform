"""This is module thing's properties handler"""

import json

from rest_framework import status
from rest_framework.decorators import api_view, permission_classes
from rest_framework.response import Response

from api.network.rpc import RpcClient
from api.serializers.thing import PropertySerializer
from api.utils.auth import AuthenticatedOnly
from api.utils.common import property_dict_to_list, property_list_to_dict
from api.utils.messaging import construct_binding_key, construct_routing_key
from api.utils.response import response_error, response_success


@api_view(['GET', 'PUT'])
@permission_classes([AuthenticatedOnly])
def get_by_id(request, thing_id):
    # List properties
    if request.method == 'GET':
        try:
            address = 'lights.{}'.format(thing_id)
            routing_key = construct_routing_key('get', request.user, address)
            binding_key = construct_binding_key(request.user, address)
            rpc = RpcClient(binding_key)
            result = rpc.call(routing_key)

            if result is not None:
                result = json.loads(result)
                properties = property_dict_to_list(result['state'])
                serializer = PropertySerializer(properties, many=True)
                return response_success(serializer.data)
            return Response(status=status.HTTP_408_REQUEST_TIMEOUT)
        except Exception as e:
            return response_error(e.__str__())

    # Update property
    elif request.method == 'PUT':
        try:
            serializer = PropertySerializer(data=request.data, many=True)
            if not serializer.is_valid():
                return response_error(serializer.errors)

            address = 'lights.{}.state'.format(thing_id)
            routing_key = construct_routing_key('put', request.user, address)
            binding_key = construct_binding_key(request.user, address)
            rpc = RpcClient(binding_key)
            data = property_list_to_dict(serializer.data)
            result = rpc.call(routing_key, data)

            if result is not None:
                result = json.loads(result)
                return response_success(result)
            return Response(status=status.HTTP_408_REQUEST_TIMEOUT)
        except Exception as e:
            return response_error(e.__str__())
