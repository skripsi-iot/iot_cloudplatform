"""This is module rule handler"""

from rest_framework.decorators import api_view, permission_classes

from api.models.rule import Rule
from api.network.messaging import emit_message
from api.serializers.rule import RuleSerializer
from api.utils.auth import AuthenticatedOnly
from api.utils.messaging import construct_routing_key
from api.utils.response import (response_error, response_success,
                                response_success_single)


@api_view(['POST', 'GET'])
@permission_classes([AuthenticatedOnly])
def index(request):
    # List rules
    if request.method == 'GET':
        rules = Rule.objects.filter(
            gateway_serial_id=request.user['gateway_serial_id']
        )
        serializer = RuleSerializer(rules, many=True)

        return response_success(serializer.data)

    # Create rule
    elif request.method == 'POST':
        request.data['gateway_serial_id'] = str(request.user['gateway_serial_id'])

        serializer = RuleSerializer(data=request.data)
        if not serializer.is_valid():
            return response_error(serializer.errors)

        try:
            rule = serializer.save()

            routing_key = construct_routing_key('post', request.user, 'rules')
            request.data.pop('gateway_serial_id')
            emit_message(routing_key, request.data, str(rule.id))
            serializer = RuleSerializer(rule)

            return response_success_single(serializer.data)
        except Exception as e:
            return response_error(e.__str__())


@api_view(['GET', 'PUT', 'DELETE'])
@permission_classes([AuthenticatedOnly])
def get_by_id(request, rule_id):
    rule = Rule.objects.filter(
        id=rule_id,
        rule_id__ne=''
    ).first()

    if rule is None:
        return response_error("Rule not found!")

    # Rule detail
    if request.method == 'GET':
        try:
            serializer = RuleSerializer(rule)
            return response_success_single(serializer.data)
        except Exception as e:
            return response_error(e.__str__())

    # Update rule
    elif request.method == 'PUT':
        request.data['gateway_serial_id'] = str(request.user['gateway_serial_id'])

        serializer = RuleSerializer(data=request.data)

        if not serializer.is_valid():
            return response_error(serializer.errors)

        try:
            updated_rule = serializer.update(rule, serializer.validated_data)

            address = 'rules.{}'.format(str(rule.rule_id))
            routing_key = construct_routing_key('put', request.user, address)
            request.data.pop('gateway_serial_id')
            emit_message(routing_key, request.data)

            serializer = RuleSerializer(updated_rule)
            return response_success_single(serializer.data)
        except Exception as e:
            return response_error(e.__str__())

    # Delete rule
    elif request.method == 'DELETE':
        try:
            rule.delete()

            address = 'rules.{}'.format(str(rule.rule_id))
            routing_key = construct_routing_key('delete', request.user, address)
            emit_message(routing_key)

            return response_success()
        except Exception as e:
            return response_error(e.__str__())
