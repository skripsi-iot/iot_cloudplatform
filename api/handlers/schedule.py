"""This is module thing handler"""

from rest_framework.decorators import api_view, permission_classes

from api.models.schedule import Schedule
from api.network.messaging import emit_message
from api.serializers.schedule import ScheduleSerializer
from api.utils.auth import AuthenticatedOnly
from api.utils.common import convert_to_utc
from api.utils.messaging import (construct_gateway_address,
                                 construct_routing_key)
from api.utils.response import (response_error, response_success,
                                response_success_single)


@api_view(['POST', 'GET'])
@permission_classes([AuthenticatedOnly])
def index(request):
    # List schedules
    if request.method == 'GET':
        schedules = Schedule.objects.filter(
            gateway_serial_id=request.user['gateway_serial_id'],
            schedule_id__ne=''
        )
        serializer = ScheduleSerializer(schedules, many=True)

        return response_success(serializer.data)

    # Create schedule
    elif request.method == 'POST':
        request.data['gateway_serial_id'] = str(request.user['gateway_serial_id'])
        request.data['command']['address'] = construct_gateway_address(
            request.data['command']['address']
        )
        serializer = ScheduleSerializer(data=request.data)

        if not serializer.is_valid():
            return response_error(serializer.errors)

        try:
            request.data['time'] = convert_to_utc(request.data['time'])
            schedule = serializer.save()

            routing_key = construct_routing_key('post', request.user, 'schedules')
            request.data.pop('gateway_serial_id')
            emit_message(routing_key, request.data, str(schedule.id))

            serializer = ScheduleSerializer(schedule)
            return response_success_single(request.data)
        except Exception as e:
            return response_error(e.__str__())


@api_view(['GET', 'PUT', 'DELETE'])
@permission_classes([AuthenticatedOnly])
def get_by_id(request, schedule_id):
    schedule = Schedule.objects.filter(
        id=schedule_id,
        schedule_id__ne=''
    ).first()

    if schedule is None:
        return response_error("Schedule not found!")

    # Schedule detail
    if request.method == 'GET':
        try:
            serializer = ScheduleSerializer(schedule)
            return response_success_single(serializer.data)
        except Exception as e:
            return response_error(e.__str__())

    # Update schedule
    elif request.method == 'PUT':
        request.data['gateway_serial_id'] = str(request.user['gateway_serial_id'])
        request.data['command']['address'] = construct_gateway_address(
            request.data['command']['address']
        )

        serializer = ScheduleSerializer(data=request.data)

        if not serializer.is_valid():
            return response_error(serializer.errors)

        try:
            request.data['time'] = convert_to_utc(request.data['time'])
            updated_rule = serializer.update(schedule, serializer.validated_data)

            address = 'schedules.{}'.format(str(schedule.schedule_id))
            routing_key = construct_routing_key('put', request.user, address)
            request.data.pop('gateway_serial_id')
            emit_message(routing_key, request.data)

            serializer = ScheduleSerializer(updated_rule)
            return response_success_single(serializer.data)
        except Exception as e:
            return response_error(e.__str__())

    # Delete schedule
    elif request.method == 'DELETE':
        try:
            schedule.delete()

            address = 'schedules.{}'.format(str(schedule.schedule_id))
            routing_key = construct_routing_key('delete', request.user, address)
            emit_message(routing_key)

            return response_success()
        except Exception as e:
            return response_error(e.__str__())
