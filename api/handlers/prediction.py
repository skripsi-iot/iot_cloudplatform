"""This is module prediction handler"""

from datetime import timedelta

from rest_framework.decorators import api_view, permission_classes

from api.models.logging import Logging
from api.prediction.core import get_prediction
from api.utils.auth import AuthenticatedOnly
from api.utils.response import response_error, response_success


@api_view(['GET'])
@permission_classes([AuthenticatedOnly])
def index(request, number_of_prediction):
    if request.method == 'GET':
        try:
            loggings = (Logging.objects
                        .filter(gateway_serial_id=request.user['gateway_serial_id'])
                        .order_by('issued_at'))
            data = [{'time': x.issued_at, 'power': x.power} for x in loggings]
            latest_time = loggings[len(loggings) - 1]['issued_at']

            prediction_results = get_prediction(loggings, int(number_of_prediction))
            predictions = []
            for predictions_result in prediction_results:
                latest_time = latest_time + timedelta(hours=1)
                predictions.append({'time': latest_time, 'power': predictions_result})
            return response_success({'data': data, 'predictions': predictions})
        except Exception as e:
            return response_error(e.__str__())
