"""This is module user handler"""

import hashlib

from rest_framework.decorators import api_view, permission_classes

from api.models.user import User
from api.serializers.user import UserAuthSerializer, UserSerializer
from api.utils.auth import AuthenticatedOnly, encode_token
from api.utils.response import (response_error, response_success,
                                response_success_single)


@api_view(['POST',])
def auth(request):
    serializer = UserAuthSerializer(data=request.data)
    if not serializer.is_valid():
        return response_error(serializer.errors)

    email = serializer.validated_data['email']
    password = serializer.validated_data['password']
    hashed_password = hashlib.md5(password.encode()).hexdigest()

    try:
        user = User.objects.get(email=email, password=hashed_password)
        serializer = UserSerializer(user)
        token = encode_token(user)

        serializer.data.token = token

        return response_success([serializer.data, {'token': token}])
    except Exception as e:
        return response_error(e.__str__())


@api_view(['GET',])
@permission_classes([AuthenticatedOnly])
def detail(request):
    try:
        serializer = UserSerializer(request.user)
        return response_success_single(serializer.data)
    except Exception as e:
        return response_error(e.__str__())


@api_view(['POST',])
def register(request):
    serializer = UserSerializer(data=request.data)

    if not serializer.is_valid():
        return response_error(serializer.errors)

    try:
        user = serializer.save()
        serializer = UserSerializer(user)
        token = encode_token(user)

        serializer.data.token = token

        return response_success_single([serializer.data, {'token': token}])
    except Exception as e:
        return response_error(e.__str__())
