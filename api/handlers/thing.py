"""This is module thing handler"""

import json

from rest_framework import status
from rest_framework.decorators import api_view, permission_classes
from rest_framework.response import Response

from api.network.messaging import emit_message
from api.network.rpc import RpcClient
from api.serializers.thing import ThingSerializer
from api.utils.auth import AuthenticatedOnly
from api.utils.common import parse_things, parse_things_response
from api.utils.messaging import construct_binding_key, construct_routing_key
from api.utils.response import (response_error, response_success,
                                response_success_single)


@api_view(['GET', 'POST'])
@permission_classes([AuthenticatedOnly])
def index(request):
    # List thing
    if request.method == 'GET':
        address = 'lights'
        routing_key = construct_routing_key('get', request.user, address)
        binding_key = construct_binding_key(request.user, address)
        rpc = RpcClient(binding_key)
        result = rpc.call(routing_key)

        if result is not None:
            result = json.loads(result)
            things = parse_things_response(result, request.user)
            serializer = ThingSerializer(things, many=True)
            return response_success(serializer.data)
        return Response(status=status.HTTP_408_REQUEST_TIMEOUT)

    # Register thing
    elif request.method == 'POST':
        address = 'lights'
        routing_key = construct_routing_key('post', request.user, address)
        binding_key = construct_binding_key(request.user, address)
        rpc = RpcClient(binding_key)
        result = rpc.call(routing_key)

        if result is not None:
            result = json.loads(result)
            return response_success(result)
        return Response(status=status.HTTP_408_REQUEST_TIMEOUT)


@api_view(['GET', 'PUT', 'DELETE'])
@permission_classes([AuthenticatedOnly])
def get_by_id(request, thing_id):
    # Thing detail
    if request.method == 'GET':
        try:
            address = 'lights.{}'.format(thing_id)
            routing_key = construct_routing_key('get', request.user, address)
            binding_key = construct_binding_key(request.user, address)
            rpc = RpcClient(binding_key)
            result = rpc.call(routing_key)

            if result is not None:
                result = json.loads(result)
                things = parse_things(thing_id, result, request.user)
                serializer = ThingSerializer(things)
                return response_success_single(serializer.data)
            return Response(status=status.HTTP_408_REQUEST_TIMEOUT)
        except Exception as e:
            return response_error(e.__str__())

    # Update thing
    elif request.method == 'PUT':
        request.data['user_id'] = str(request.user['id'])

        serializer = ThingSerializer(data=request.data)

        if not serializer.is_valid():
            return response_error(serializer.errors)

        try:
            address = 'lights.{}'.format(thing_id)
            routing_key = construct_routing_key('put', request.user, address)
            binding_key = construct_binding_key(request.user, address)
            rpc = RpcClient(binding_key)
            result = rpc.call(routing_key, serializer.validated_data)

            if result is not None:
                result = json.loads(result)
                return response_success(result)
            return Response(status=status.HTTP_408_REQUEST_TIMEOUT)
        except Exception as e:
            return response_error(e.__str__())

    # Delete thing
    elif request.method == 'DELETE':
        try:
            address = 'lights.{}'.format(thing_id)
            routing_key = construct_routing_key('delete', request.user, address)
            emit_message(routing_key)
            return response_success()
        except Exception as e:
            return response_error(e.__str__())
