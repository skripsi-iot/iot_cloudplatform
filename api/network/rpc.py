"""This is module RPC functionality"""

import json
import os
import time
import uuid

import pika


class RpcClient(object):

    def __init__(self, binding_key):
        self.TIME_OUT = 10.0
        self.connection = pika.BlockingConnection(
            pika.URLParameters(os.environ.get('PIKA_URLPARAMETERS'))
        )

        self.channel = self.connection.channel()

        self.channel.exchange_declare(exchange='response', exchange_type='topic')

        result = self.channel.queue_declare('', exclusive=True)
        self.callback_queue = result.method.queue

        self.channel.queue_bind(
            exchange='response', queue=self.callback_queue, routing_key=binding_key
        )

        self.channel.basic_consume(
            queue=self.callback_queue,
            on_message_callback=self.on_response,
            auto_ack=True)

    def on_response(self, ch, method, props, body):
        if self.corr_id == props.correlation_id:
            self.response = body
            ch.close()

    def call(self, routing_key='anonymous.info', data={}):
        self.response = None
        self.corr_id = str(uuid.uuid4())

        starting_time = time.time()
        elapsed_time = 0.0
        message = json.dumps(data)

        self.channel.basic_publish(
            exchange='command',
            routing_key=routing_key,
            properties=pika.BasicProperties(
                reply_to=self.callback_queue,
                correlation_id=self.corr_id,
                timestamp=int(starting_time)
            ),
            body=message)

        while self.response is None and (elapsed_time < self.TIME_OUT):
            self.connection.process_data_events()
            elapsed_time = time.time() - starting_time
        return self.response
