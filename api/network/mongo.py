"""This is module MongoDB connection functionality"""

import os

from mongoengine import connect


def connect_db():
    connect(
        db=os.environ.get('DB_NAME'),
        host=os.environ.get('DB_HOST'),
        username=os.environ.get('DB_USERNAME'),
        password=os.environ.get('DB_PASSWORD')
    )
