"""This is module asynchronous AMQP messaging functionality"""

import json
import os

import pika

from api.handlers.messaging import handle_response


def emit_message(routing_key='anonymous.info', data={}, corr_id=None):
    connection = pika.BlockingConnection(
        pika.URLParameters(os.environ.get('PIKA_URLPARAMETERS'))
    )
    channel = connection.channel()

    channel.exchange_declare(exchange='command', exchange_type='topic')

    message = json.dumps(data)
    channel.basic_publish(
        exchange='command',
        routing_key=routing_key,
        body=message,
        properties=pika.BasicProperties(
            delivery_mode=2,
            correlation_id=corr_id
        )
    )
    print(" [x] Sent %r:%r" % (routing_key, message))
    channel.close()
    connection.close()


def on_response_received(ch, method, properties, body):
    print(" [x] %r:%r" % (method.routing_key, body))

    data = json.loads(body)
    correlation_id = properties.correlation_id
    routing_key = str(method.routing_key).split('.')

    handle_response(data, correlation_id, routing_key)


def receive_response(binding_keys=['#.response']):
    connection = pika.BlockingConnection(
        pika.URLParameters(os.environ.get('PIKA_URLPARAMETERS'))
    )
    channel = connection.channel()

    channel.exchange_declare(exchange='response', exchange_type='topic')

    result = channel.queue_declare('', exclusive=True)
    queue_name = result.method.queue

    for binding_key in binding_keys:
        channel.queue_bind(
            exchange='response',
            queue=queue_name,
            routing_key=binding_key
        )

    print(' [*] Waiting for logs. To exit press CTRL+C')

    channel.basic_consume(
        queue=queue_name,
        on_message_callback=on_response_received,
        auto_ack=True
    )

    channel.start_consuming()
