"""This is module user models"""

from datetime import datetime

from mongoengine import Document
from mongoengine.fields import DateTimeField, StringField


class User(Document):
    email = StringField(unique=True)
    name = StringField(max_length=20, required=True)
    password = StringField(required=True)
    gateway_serial_id = StringField(required=True)
    created = DateTimeField(default=datetime.now)
    updated = DateTimeField(default=datetime.now)
