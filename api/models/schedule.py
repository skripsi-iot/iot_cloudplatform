"""This is module schedule models"""

from datetime import datetime

from mongoengine import Document, EmbeddedDocument
from mongoengine.fields import (DateTimeField, DictField, EmbeddedDocumentField,
                                StringField)


class Command(EmbeddedDocument):
    address = StringField(max_length=100, required=True, default='')
    method = StringField(max_length=100, required=True, default='PUT')
    body = DictField(required=True)


class Schedule(Document):
    gateway_serial_id = StringField(required=True)
    schedule_id = StringField(default='')
    name = StringField(max_length=100)
    description = StringField(max_length=250)
    command = EmbeddedDocumentField(Command)
    time = StringField(required=True)
    status = StringField(default='enabled')
    schedule_state = StringField(default='inactive')
    created = DateTimeField(default=datetime.now)
    updated = DateTimeField(default=datetime.now)
