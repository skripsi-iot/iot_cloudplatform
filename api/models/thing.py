"""This is module thing models"""

from datetime import datetime

from mongoengine import Document, EmbeddedDocument
from mongoengine.fields import (DateTimeField, DynamicField,
                                EmbeddedDocumentField, ListField, StringField)


class Property(EmbeddedDocument):
    name = StringField(max_length=32, required=True, default='')
    value = DynamicField()


class Thing(Document):
    name = StringField(max_length=100, required=True)
    gateway_serial_id = StringField(max_length=100)
    properties = ListField(EmbeddedDocumentField(Property))
    created = DateTimeField(default=datetime.now)
    updated = DateTimeField(default=datetime.now)
