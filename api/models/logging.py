from datetime import datetime

from mongoengine import Document
from mongoengine.fields import *


class Logging(Document):
    issued_at = DateTimeField(default=datetime.now())
    gateway_serial_id = StringField(required=True)
    power = DecimalField(required=True)
