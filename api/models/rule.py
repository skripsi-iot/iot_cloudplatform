"""This is module rule models"""

from datetime import datetime

from mongoengine import Document, EmbeddedDocument
from mongoengine.fields import (DateTimeField, DictField,
                                EmbeddedDocumentField, IntField, ListField,
                                StringField)


class Action(EmbeddedDocument):
    address = StringField(max_length=100, required=True, default='')
    body = DictField(required=True)
    method = StringField(max_length=100, required=True, default='')


class Condition(EmbeddedDocument):
    address = StringField(max_length=100, required=True, default='')
    operator = StringField(max_length=100, required=True, default='')
    value = StringField(max_length=100, required=True, default='')


class Rule(Document):
    gateway_serial_id = StringField(required=True)
    rule_id = StringField(default='')
    name = StringField(max_length=100, required=True)
    actions = ListField(EmbeddedDocumentField(Action))
    conditions = ListField(EmbeddedDocumentField(Condition))
    periodic = IntField(default=0)
    rule_state = StringField(default='inactive')
    status = StringField(default='enabled')
    created = DateTimeField(default=datetime.now)
    updated = DateTimeField(default=datetime.now)
