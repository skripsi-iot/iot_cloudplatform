"""This is main module for predictive analysis"""

import os
from math import sqrt

from keras.models import load_model
from numpy import array
from pandas import DataFrame, Series, concat, datetime
from sklearn.preprocessing import MinMaxScaler


def parser(date):
    """Date-time parsing function for loading the dataset"""
    return datetime.strptime(date, '%Y-%m-%d %H:%M:%S')


def series_to_supervised(data, n_in=1, n_out=1, dropnan=True):
    """Convert time series into supervised learning problem"""
    n_vars = 1 if type(data) is list else data.shape[1]
    df = DataFrame(data)
    cols, names = list(), list()
    # input sequence (t-n, ... t-1)
    for i in range(n_in, 0, -1):
        cols.append(df.shift(i))
        names += [('var%d(t-%d)' % (j+1, i)) for j in range(n_vars)]
    # forecast sequence (t, t+1, ... t+n)
    for i in range(0, n_out):
        cols.append(df.shift(-i))
        if i == 0:
            names += [('var%d(t)' % (j+1)) for j in range(n_vars)]
        else:
            names += [('var%d(t+%d)' % (j+1, i)) for j in range(n_vars)]
    # put it all together
    agg = concat(cols, axis=1)
    agg.columns = names
    # drop rows with NaN values
    if dropnan:
        agg.dropna(inplace=True)
    return agg


def difference(dataset, interval=1):
    """Create a differenced series"""
    diff = list()
    for i in range(interval, len(dataset)):
        value = dataset[i] - dataset[i - interval]
        diff.append(value)
    return Series(diff)


def prepare_data(series, n_lag, n_seq):
    """Transform series into train and test sets for supervised learning"""
    # extract raw values
    raw_values = series.values
    # transform data to be stationary
    diff_series = difference(raw_values, 1)
    diff_values = diff_series.values
    diff_values = diff_values.reshape(len(diff_values), 1)
    # rescale values to -1, 1
    scaler = MinMaxScaler(feature_range=(-1, 1))
    scaled_values = scaler.fit_transform(diff_values)
    scaled_values = scaled_values.reshape(len(scaled_values), 1)
    # transform into supervised learning problem X, y
    supervised = series_to_supervised(scaled_values, n_lag, n_seq)
    supervised_values = supervised.values

    train = supervised_values[0:]
    return scaler, train


def forecast_lstm(model, X, n_batch, n_seq):
    # reshape input pattern to [samples, timesteps, features]
    X = X.reshape(1, 1, len(X))
    # make forecast
    forecast = model.predict(X, batch_size=n_batch)
    # convert to array
    return [x for x in forecast[0, :n_seq]]


def make_forecasts(model, n_batch, train, n_lag, n_seq):
    forecasts = list()
    for i in range(n_seq):
        X, y = train[len(train)-1, 0:n_lag], train[len(train)-1, n_lag:]
        # make forecast
        forecast = forecast_lstm(model, X, n_batch, n_seq)
        # store the forecast
        forecasts.append(forecast)
    return forecasts


def inverse_difference(last_ob, forecast):
    """Invert differenced forecast"""
    # invert first forecast
    inverted = list()
    inverted.append(forecast[0] + last_ob)
    # propagate difference forecast using inverted first value
    for i in range(1, len(forecast)):
        inverted.append(forecast[i] + inverted[i-1])
    return inverted


def inverse_transform(series, forecasts, scaler, n_test):
    """Inverse data transform on forecasts"""
    inverted = list()
    for i in range(len(forecasts)):
        # create array from forecast
        forecast = array(forecasts[i])
        forecast = forecast.reshape(1, len(forecast))
        # invert scaling
        inv_scale = scaler.inverse_transform(forecast)
        inv_scale = inv_scale[0, :]
        # invert differencing
        index = len(series) - n_test + i
        last_ob = series.values[index]
        inv_diff = inverse_difference(last_ob, inv_scale)
        # store
        inverted.append(inv_diff)
    return inverted

def parse_forecasts(series, forecasts, n_test):
    i = len(forecasts) - 1
    yaxis = forecasts[i]
    return yaxis


def get_prediction(data, number_of_prediction=1):
    """Get forward prediction of current data"""
    dates = [value.issued_at for value in data]
    powers = [float(value.power) for value in data]

    data = {'issued_at': dates, 'power': powers}
    dataframe = DataFrame(data)
    dataframe.set_index('issued_at')

    series = Series(dataframe['power'], index=dataframe.index)

    n_lag = 1
    n_seq = number_of_prediction
    n_batch = 1

    scaler, train = prepare_data(series, n_lag, n_seq)

    # load model
    module_dir = os.path.dirname(__file__)
    file_path = os.path.join(module_dir, 'model.h5')
    model = load_model(file_path)

    # make forecasts
    forecasts = make_forecasts(model, n_batch, train, n_lag, n_seq)

    # inverse transform forecasts
    forecasted = inverse_transform(series, forecasts, scaler, n_seq)

    forecasted = parse_forecasts(series, forecasted, n_seq)

    return forecasted
