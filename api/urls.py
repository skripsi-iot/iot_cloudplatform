"""iot_cloudplatform URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.urls import include

from api.handlers import prediction, properties, rule, schedule, thing, user

urlpatterns = [
    url(r'^thing/$', thing.index, name='thing-index'),
    url(r'^thing/([0-9]{1,2})/$', thing.get_by_id, name='thing-by-id'),
    url(r'^property/([0-9]{1,2})/$', properties.get_by_id, name='property-get-by_id'),
    url(r'^rule/$', rule.index, name='rule-index'),
    url(r'^rule/([0-9a-f]{24})/$', rule.get_by_id, name='rule-by-id'),
    url(r'^schedule/$', schedule.index, name='schedule-index'),
    url(r'^schedule/([0-9a-f]{24})/$', schedule.get_by_id, name='schedule-by-id'),
    url(r'^user/', include([
        url(r'^auth/$', user.auth, name='authenticate'),
        url(r'^register/$', user.register, name='register'),
        url(r'^detail/$', user.detail, name='detail'),
    ]), name='user'),
    url(r'^power/prediction/(\d{1,2})/$', prediction.index, name='prediction-index'),
]
