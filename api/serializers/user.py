"""This is module user serializers"""

import hashlib
from datetime import datetime

from rest_framework_mongoengine.serializers import (DocumentSerializer,
                                                    serializers)

from api.models.user import User


class UserSerializer(DocumentSerializer):
    id = serializers.CharField(required=False)

    def create(self, validated_data):
        password = validated_data['password']
        hashed_password = hashlib.md5(password.encode()).hexdigest()
        created_instance = User(
            name=validated_data['name'],
            email=validated_data['email'],
            password=hashed_password,
            gateway_serial_id=validated_data['gateway_serial_id']
        )
        created_instance.save()
        return created_instance

    def update(self, instance, validated_data):
        updated_instance = super(UserSerializer, self).update(instance, validated_data)
        updated_instance.updated = datetime.now()
        updated_instance.save()
        return updated_instance

    class Meta:
        model = User
        fields = ('id', 'email', 'name', 'password', 'gateway_serial_id')
        extra_kwargs = {
            'id': {'read_only': True},
            'password': {'write_only': True},
        }


class UserAuthSerializer(DocumentSerializer):
    email = serializers.EmailField(required=False)
    password = serializers.CharField(required=False)

    class Meta:
        model = User
        fields = ('email', 'password')
