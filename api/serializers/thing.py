"""This is module thing serializer"""

from datetime import datetime

from rest_framework_mongoengine.serializers import (DocumentSerializer,
                                                    serializers)

from api.models.thing import Property, Thing


class PropertySerializer(DocumentSerializer):
    class Meta:
        model = Property
        fields = ('name', 'value')


class ThingSerializer(DocumentSerializer):
    id = serializers.CharField(required=False)
    properties = PropertySerializer(required=False, many=True)

    class Meta:
        model = Thing
        fields = ('id', 'gateway_serial_id', 'name', 'properties')
        read_only_fields = ('id',)
