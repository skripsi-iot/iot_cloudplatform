"""This is module rule serializers"""

from datetime import datetime

from rest_framework_mongoengine.serializers import (DocumentSerializer,
                                                    serializers)

from api.models.schedule import Command, Schedule


class CommandSerializer(DocumentSerializer):
    class Meta:
        model = Command
        fields = ('address', 'body', 'method')


class ScheduleSerializer(DocumentSerializer):
    id = serializers.CharField(required=False)
    command = CommandSerializer()

    def create(self, validated_data):
        schedule_command = validated_data.pop('command')
        created_instance = Schedule(
            gateway_serial_id=validated_data['gateway_serial_id'], 
            name=validated_data['name'],
            description=validated_data['description'],
            time=validated_data['time'],
            status=validated_data['status']
        )

        created_instance.command = Command(**schedule_command)

        created_instance.save()
        return created_instance

    def update(self, instance, validated_data):
        schedule_command = validated_data.pop('command')
        updated_instance = super(ScheduleSerializer, self).update(instance, validated_data)

        updated_instance.command = Command(**schedule_command)

        updated_instance.schedule_state = instance.schedule_state
        updated_instance.updated = datetime.now()
        updated_instance.save()
        return updated_instance

    class Meta:
        model = Schedule
        fields = ('id', 'gateway_serial_id', 'name',
                  'description', 'command', 'status',
                  'time', 'schedule_state')
        read_only_fields = ('id',)
