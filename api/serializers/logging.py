"""This is module logging serializers"""


from rest_framework_mongoengine.serializers import DocumentSerializer

from api.models.logging import Logging


class LoggingSerializer(DocumentSerializer):

    def create(self, validated_data):
        created_instance = Logging(
            gateway_serial_id=validated_data['gateway_serial_id'],
            issued_at=validated_data['issued_at'],
            power=validated_data['power']
        )

        created_instance.save()
        return created_instance


    class Meta:
        model = Logging
        fields = ('issued_at', 'power')
