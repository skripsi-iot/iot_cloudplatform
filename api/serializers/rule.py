"""This is module rule serializers"""

from datetime import datetime

from rest_framework_mongoengine.serializers import (DocumentSerializer,
                                                    serializers)

from api.models.rule import Action, Condition, Rule


class ActionSerializer(DocumentSerializer):
    class Meta:
        model = Action
        fields = ('address', 'body', 'method')


class ConditionSerializer(DocumentSerializer):
    class Meta:
        model = Condition
        fields = ('address', 'operator', 'value')


class RuleSerializer(DocumentSerializer):
    id = serializers.CharField(required=False)
    actions = ActionSerializer(many=True)
    conditions = ConditionSerializer(many=True)

    def create(self, validated_data):
        rule_actions = validated_data.pop('actions')
        rule_conditions = validated_data.pop('conditions')
        created_instance = Rule(
            gateway_serial_id=validated_data['gateway_serial_id'],
            name=validated_data['name']
        )

        for action in rule_actions:
            created_instance.actions.append(Action(**action))

        for condition in rule_conditions:
            created_instance.conditions.append(Condition(**condition))

        created_instance.save()
        return created_instance

    def update(self, instance, validated_data):
        rule_actions = validated_data.pop('actions')
        rule_conditions = validated_data.pop('conditions')
        updated_instance = super(RuleSerializer, self).update(instance, validated_data)

        updated_instance.actions = []
        for action in rule_actions:
            updated_instance.actions.append(Action(**action))

        updated_instance.conditions = []
        for condition in rule_conditions:
            updated_instance.conditions.append(Condition(**condition))

        updated_instance.rule_state = instance.rule_state
        updated_instance.updated = datetime.now()
        updated_instance.save()
        return updated_instance

    class Meta:
        model = Rule
        fields = (
            'id', 'gateway_serial_id',
            'name', 'actions',
            'conditions', 'status',
            'rule_state'
        )
        read_only_fields = ('id', 'rule_state')
