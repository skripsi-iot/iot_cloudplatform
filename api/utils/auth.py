"""This is module authentication utils"""

import datetime
import os

import jwt
from rest_framework import permissions

from api.models.user import User


def encode_token(user):
    token_lifespan = datetime.datetime.now() + datetime.timedelta(days=1)
    token = jwt.encode(
        {
            'id': str(user['id']),
            'gateway_serial_id': str(user['gateway_serial_id']), 
            'exp': token_lifespan
        },
        os.environ.get('AUTH_PRIVATE_KEY'), 
        algorithm='HS256'
    ).decode('utf-8')

    return token


def decode_token(token):
    data = jwt.decode(token, os.environ.get('AUTH_PRIVATE_KEY'), algorithms=['HS256'])
    user = User.objects.filter(id=data['id']).first()
    access_granted = user is not None
    return access_granted, user


class AuthenticatedOnly(permissions.BasePermission):

    def login_status(self, request):
        token = request.META.get('HTTP_AUTHORIZATION')
        token = token[7:]
        access_granted, user = decode_token(token)
        return access_granted, user


    def has_permission(self, request, view):
        try:
            access_granted, user = self.login_status(request)
            request.user = None
            if access_granted:
                request.user = user
                return True
            else:
                return False
        except Exception:
            return False
