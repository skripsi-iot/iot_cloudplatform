"""This is module response utils"""

from rest_framework.response import Response


def response_success(data=[]):
    response = {'status': 0, 'message': 'success', 'data': data}
    return Response(response)

def response_success_single(data):
    return response_success([data])

def response_error(message):
    response = {'status': -1, 'message': message, 'data': []}
    return Response(response)
