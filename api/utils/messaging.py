"""This is module messaging utils"""

import os


def construct_routing_key(method, user, routing):
    """Format [METHOD].[GW_ID].[API].<routing>"""
    routing_key = '{}.{}.{}.{}'.format(
        method,
        str(user['gateway_serial_id']),
        os.environ.get('GATEWAY_API_KEY'),
        routing
    )
    return routing_key


def construct_binding_key(user, routing_key):
    """Format [GW_ID].<routing>"""
    binding_key = '{}.{}.response'.format(
        str(user['gateway_serial_id']),
        routing_key
    )
    return binding_key


def construct_gateway_address(address):
    """Format [METHOD].[GW_ID].[API].<routing>"""
    address = '/api/{}{}'.format(
        os.environ.get('GATEWAY_API_KEY'),
        address
    )
    return address
