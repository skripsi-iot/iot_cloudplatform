"""This is module common utils"""

from datetime import datetime

import re

def property_list_to_dict(l):
    d = {x['name']: x['value'] for x in l}
    return d


def property_dict_to_list(d):
    l = [{'name': k, 'value': v} for k, v in d.items()]
    return l


def parse_things_response(response, user):
    things = []
    for k, v in response.items():
        thing = parse_things(k, v, user)
        things.append(thing)
    return things


def parse_things(id, value, user):
    thing = {
        'id': id,
        'name': value['name'],
        'gateway_serial_id': str(user['gateway_serial_id']),
        'properties': property_dict_to_list(value['state'])
    }
    return thing


def convert_to_utc(time_str):
    offset = datetime.utcnow() - datetime.now()

    if (
            # Everyday regex
            re.match(r'W([0-9]{1,3})/T(\d\d):(\d\d):(\d\d)', time_str) or

            # Recurring timer regex
            re.match(r'R([0-9]{0,2})/PT(\d\d):(\d\d):(\d\d)', time_str) or

            # Timer expiring regex
            re.match(r'PT(\d\d):(\d\d):(\d\d)', time_str)
        ):
        local_time = time_str[-8:]
        local_datetime = datetime.strptime(local_time, "%H:%M:%S")
        result_utc_datetime = local_datetime + offset
        result_utc_datetime = result_utc_datetime.strftime("%H:%M:%S")
        result_utc_datetime = '{}{}'.format(time_str[:-8], result_utc_datetime)
        return result_utc_datetime

    else:
        local_datetime = datetime.strptime(time_str, "%Y-%m-%dT%H:%M:%S")
        result_utc_datetime = local_datetime + offset
        result_utc_datetime = result_utc_datetime.strftime("%Y-%m-%dT%H:%M:%S")
        return result_utc_datetime
