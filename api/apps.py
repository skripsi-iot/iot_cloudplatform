from django.apps import AppConfig

class apiConfig(AppConfig):
    name = 'api'
    label = 'api'
    verbose_name = 'Smart Generic IoT Cloud Platform'